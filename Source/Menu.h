#pragma once

#include <SFML/Graphics.hpp>		

namespace font
{
namespace size
{
const sf::Vector2f small = { 0.5f, 0.5f };
const sf::Vector2f medium = { 0.7f, 0.7f };
const sf::Vector2f large = { 1.2f, 1.2f };
const sf::Vector2f dynamic = {};
}
}

class Menu
{
public:
    Menu(sf::RenderTarget& target, bool isNewGame);
    ~Menu();
    void input();
    int update();
    void show(sf::RenderTarget& target);
private:
    static const int num_elements = 3;	// number of menu items
    sf::Font font;
    sf::Text title;
    sf::Text text_menu[num_elements];

    int marked_item;			// number of the marked item in the menu
    int selected_item;			// number of the selected item in the menu
};
