#pragma once

#include <string>
#include <fstream>
#include <SFML/System/Vector2.hpp>

static const std::string filename = "settings.ini";

class Settings
{
    static std::fstream file;

    bool vertsync;
    bool fullscreen;
    sf::Vector2i resolution;

    void setDefault();

public:
    Settings();
    ~Settings();

    bool getVertsync() const { return vertsync; }
    bool getFullscreen() const { return fullscreen; }
    sf::Vector2i getResolution() const { return resolution; }

    void setVertsync(bool vertsync) { this->vertsync = vertsync; }
    void setFullscreen(bool fullscreen) { this->fullscreen = fullscreen; }
    void setResolution(sf::Vector2i resolution) { this->resolution = resolution; }
};
