set(SOURCES
  main.cpp
  Bullet.cpp
  Game.cpp
  Level.cpp
  Menu.cpp
  Object.cpp
  Player.cpp
  Records.cpp
  ResourceManager.cpp
  Settings.cpp)

set(HEADERS
  Bullet.h
  Game.h
  Level.h
  Menu.h
  Object.h
  Player.h
  Records.h
  ResourceManager.h
  Settings.h)

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules" ${CMAKE_MODULE_PATH})
find_package(SFML REQUIRED system window graphics audio)
find_package(OpenGL REQUIRED)
include_directories(${SFML_INCLUDE_DIR} ${OPENGL_INCLUDE_DIRS})

add_executable(asteroids-0x7e4 ${SOURCES} ${HEADERS})
target_link_libraries(asteroids-0x7e4 ${SFML_LIBRARIES} ${OPENGL_LIBRARIES})
