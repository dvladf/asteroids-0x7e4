#include "ResourceManager.h"
#include <stdexcept>

ResourceManager::ResourceManager()
{
    if (!textureAsteroid[0].loadFromFile("Data/asteroid1.png") ||
        !textureAsteroid[1].loadFromFile("Data/asteroid2.png") ||
        !textureAsteroid[2].loadFromFile("Data/asteroid3.png") ||
        !textureAsteroid[3].loadFromFile("Data/asteroid4.png") ||
        !textureAsteroid[4].loadFromFile("Data/asteroid5.png") ||
        !textureAsteroid[5].loadFromFile("Data/asteroid6.png") ||
        !textureAsteroid[6].loadFromFile("Data/asteroid7.png") ||
        !textureShip.loadFromFile("Data/playerShip.png") ||
        !textureBackground.loadFromFile("Data/background.png") ||
        !textureBullet.loadFromFile("Data/laserBlue01.png"))
    {
        throw std::runtime_error("Loading data files is failed");
    };
}
