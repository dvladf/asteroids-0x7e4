#include "Player.h"
#include <cmath>

Player::Player(sf::RenderTarget& target, ResourceManager& resourceManager ) : resourceManager(resourceManager)
{
    isAccleration = false;
    accleration = 0; angle = 0;

    sf::Texture& texture = resourceManager.textureShip;
    
    player.setScale(target.getSize().x / 4200.f, target.getSize().x / 4200.f);
    player.setTexture(texture);
    player.setOrigin(texture.getSize().x / 2, texture.getSize().y / 2);
    player.setPosition(target.getSize().x / 2, target.getSize().y / 2);

    shotSoundBuffer.loadFromFile("Data/laser.wav");
    shotSound.setBuffer(shotSoundBuffer);
    shotSound.setVolume(10);
}

Player::~Player()
{
    for (int i = 0; i < bullets.size(); i++) {
        delete bullets[i];
        bullets[i] = nullptr;
    }
}

void Player::input()
{
    angle = 0; isAccleration = false;

    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::W)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))) {
        isAccleration = true;
    }

    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::A)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))) {
        angle = -DEF_ANGLE;
    }

    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::D)) || (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))) {
        angle = DEF_ANGLE;
    }

    if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Space))){
        if (checkShot)
            shot();
    } else {
        checkShot = true;
    }

}

void Player::update(float frametime)
{
    if (isAccleration) {
        accleration = DEF_SPEED * frametime;
    } else {
        accleration = 0;
    }

    player.rotate(angle * frametime);

    movement.x += accleration * sin(player.getRotation() * DEG2RAD);
    movement.y -= accleration * cos(player.getRotation() * DEG2RAD);

    player.move(movement * frametime);

    for (size_t i = 0; i < bullets.size(); i++) {
        bullets[i]->update(frametime);
    }
}

void Player::show(sf::RenderTarget& target)
{
    target.draw(player);
    checkFrontierBullets(target);
    for (size_t i = 0; i < bullets.size(); i++) {
        bullets[i]->show(target);
    }
}

void Player::shot()
{
    Bullet *bullet = new Bullet(player.getPosition(), player.getRotation(), resourceManager);
    bullets.push_back(bullet);
    shotSound.play();
    checkShot = false;
}

void Player::checkFrontierBullets(sf::RenderTarget& target)
{
    for (int i = 0; i < bullets.size(); i++) {
        if (bullets[i]->getSprite().getPosition().x > target.getSize().x || bullets[i]->getSprite().getPosition().x < 0 ||
            bullets[i]->getSprite().getPosition().y > target.getSize().y || bullets[i]->getSprite().getPosition().y < 0) {
            eraseBullet(i);
        }
    }
}

void Player::eraseBullet(int i)
{
    delete bullets[i];
    bullets[i] = nullptr;
    bullets.erase(bullets.begin() + i);
}

// getters
sf::Sprite Player::getSprite() const{
    return player;
}

sf::Vector2f Player::getMovement() const{
    return movement;
}

std::vector<Bullet*> Player::getBullets() const{
    return bullets;
}

// setters
void Player::setSprite(sf::Sprite change_sprite){
    player = change_sprite;
}

void Player::setMovement(sf::Vector2f change_movement){
    movement = change_movement;
}
