#include <fstream>
#include <sstream>
#include <cstring>

#include <SFML/Graphics.hpp>


class Records
{
public:
    Records(sf::RenderTarget& target);
    ~Records();
    void show(sf::RenderTarget& target);
    void add(std::string name, int score);
    int  getMinScore() const;
    void writeData();

private:
    static const int num_elements = 10;

    std::ifstream in;
    std::ofstream out;

    struct record {
        int  score;
        char name[10];
    } records[num_elements] {};

    bool isModification;
    sf::Font font;

    sf::Text tableRecordsName[num_elements];
    sf::Text tableRecordsScore[num_elements];
};
