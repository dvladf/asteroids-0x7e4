#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "Player.h"
#include "ResourceManager.h"
#include "Object.h"

#include <sstream>
#include <cmath>


class Level
{
public:
    Level(sf::RenderTarget& target, ResourceManager& resourceManager);
    ~Level();
    void input();
    int update(sf::RenderTarget& target, float frametime);
    void show(sf::RenderTarget& target);
    bool playerIsLive();
    bool objectIsLoading();
    void loadObjects(sf::RenderTarget& target);

private:
    sf::Sprite checkFrontier(sf::RenderTarget& target, sf::Sprite sprite);
    void checkCollision(sf::RenderTarget& target);

private:
    ResourceManager& resourceManager;
    
    static const int countObjects = 100;
    int currentCountObjects;
    Player* player;
    Object* objects[countObjects];
    int currentItemObjects;
    int itemObjects;

    int score;
    int time_collision;
    sf::Clock clock;
    sf::Time time;
    int rand_time;

    sf::SoundBuffer hitSoundBuffer;
    sf::Sound hitSound;

    bool isLoading;
};
