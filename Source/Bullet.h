#include <SFML/Graphics.hpp>
#include <vector>

#include "ResourceManager.h"

#define DEG2RAD 3.14159f / 180.0f

class Bullet
{
public:
    Bullet(sf::Vector2f positionPlayer, float angle, ResourceManager& resourceManager);
    ~Bullet();
    void show(sf::RenderTarget& target);
    void update(float frametime);

    sf::Sprite getSprite() const;

private:
    sf::Sprite bullet;
    sf::Vector2f movementBullet;
};
