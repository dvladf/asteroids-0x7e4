#include "Bullet.h"
#include <cmath>

Bullet::Bullet(sf::Vector2f positionPlayer, float angle, ResourceManager& resourceManager)
{
    sf::Texture& texture = resourceManager.textureBullet;
    
    bullet.setTexture(texture);
    bullet.setOrigin(texture.getSize().x / 2, texture.getSize().y / 2);
    bullet.setPosition(positionPlayer);
    bullet.setRotation(angle);
    bullet.setScale(0.6, 0.6);

    movementBullet.x = sin(bullet.getRotation() * DEG2RAD);
    movementBullet.y = -cos(bullet.getRotation() * DEG2RAD);
}

Bullet::~Bullet() {
}

sf::Sprite Bullet::getSprite() const {
    return bullet;
}

void Bullet::show(sf::RenderTarget& target){
    target.draw(bullet);
}

void Bullet::update(float frametime){
    bullet.move(movementBullet * 2.f * frametime);
}
