#include "Menu.h"

Menu::Menu(sf::RenderTarget& target, bool isNewGame)
{
    marked_item = 0;
    selected_item = -1;

    if (!font.loadFromFile("Data/kenvector_future.ttf")) {
        // error... File is not found
    }

    title.setString("Asteroids\t\t2020");
    title.setFont(font);
    title.setScale(target.getSize().x / 800.f, target.getSize().x / 800.f);
    title.setColor(sf::Color::Yellow);
    title.setOrigin((title.getLocalBounds().width) / 2, (title.getLocalBounds().height) / 2);
    title.setPosition(target.getSize().x / 2, target.getSize().y / 3);

    if (isNewGame) {
        text_menu[0].setString("New Game");
    } else {
        text_menu[0].setString("Continue");
    }
    
    text_menu[1].setString("Records");
    text_menu[2].setString("Quit");

    for (size_t i = 0; i < num_elements; i++) {
        text_menu[i].setFont(font);
        text_menu[i].setScale(1.2, 1.2);
        text_menu[i].setOrigin((text_menu[i].getLocalBounds().width)/2, (text_menu[i].getLocalBounds().height)/2);
        text_menu[i].setPosition(target.getSize().x / 2, target.getSize().y / 1.5+i*text_menu[i].getLocalBounds().height*2.3);
    }
}

Menu::~Menu()
{
}

void Menu::input()
{
    selected_item = -1;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && marked_item < num_elements - 1) {
        marked_item++;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && marked_item > 0) {
        marked_item--;
    }

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
        selected_item = marked_item;
    }
}

int Menu::update()
{
    for (int i = 0; i < num_elements; i++)
	text_menu[i].setColor(sf::Color::White);

    text_menu[marked_item].setColor(sf::Color::Red);
    return selected_item;
}

void Menu::show(sf::RenderTarget& target)
{
    target.draw(title);

    for (size_t i = 0; i < num_elements; i++) {
        target.draw(text_menu[i]);
    }
}
