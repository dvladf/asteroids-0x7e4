#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>

#define DEG2RAD 3.14159f / 180.0f

class Object
{
public:
    virtual ~Object();

    void show(sf::RenderTarget& target);
    void add(sf::RenderTarget& target);

    sf::Sprite getSprite() const;
    sf::Vector2f getMovement() const;
    int getLife() const;
    int getPoints() const;
    bool getVisible() const;

    void setSprite(sf::Sprite change_sprite);
    void setMovement(sf::Vector2f change_movement);
    void setLife(int change_life);

    virtual void update(float frametime) = 0;

private:
    sf::Texture textureObject;
    sf::Sprite spriteObject;
    sf::Vector2f movementObject;
    float scaleObject;
    int lifeObject;
    int pointsObject;
    bool visibleObject;

protected:
    Object();
    float fRand(float min, float max);

    void setScale(float change_scale);
    void setTexture(sf::Texture change_texture);
    void setPoints(int change_points);
};

class Asteroid : public Object
{
public:
    Asteroid(sf::Texture change_texture[]);
    ~Asteroid();
    void update(float frametime);
private:
    float speed, rotate;
    static const float min_speed;
    static const float max_speed;
    static const float min_rotate;
    static const float max_rotate;
};

class BigAsteroid : public Asteroid
{
public:
    BigAsteroid(sf::Texture change_texture[]);
    ~BigAsteroid();
private:
    static const int   life;
    static const int   points;
    static const float scale;
};

class MedAsteroid : public Asteroid
{
public:
    MedAsteroid(sf::Texture change_texture[]);
    ~MedAsteroid();
private:
    static const int   life;
    static const int   points;
    static const float scale;
};

class SmallAsteroid : public Asteroid
{
public:
    SmallAsteroid(sf::Texture change_texture[]);
    ~SmallAsteroid();
private:
    static const int   life;
    static const int   points;
    static const float scale;
};
