#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

#include "Menu.h"
#include "Level.h"
#include "Records.h"
#include "Settings.h"

namespace app
{
constexpr char version[] = "ver\t0.1";
constexpr int  fps = 500;
}

class Game
{
public:
    Game(Settings settings);
    ~Game();
    void run();

private:
    bool runMenu();
    void runLevel();
    void runOption();
    void runGameOver();
    void runAddRecord();
    void runTableRecords();
    Level*   level = nullptr;
    Records* records = nullptr;

private:
    //sf::Texture backgroundTexture;
    ResourceManager resourceManager;
    sf::Sprite  background;

    sf::Font font;
    sf::Text text_version;
    sf::Text text_score;
    sf::Text text_loading;

    sf::Music music;

    sf::SoundBuffer loseSoundBuffer;
    sf::Sound       loseSound;

    sf::Clock clock;
    bool isNewGame;
    int win_width, win_height;
    int score;
    sf::RenderWindow window;
};
