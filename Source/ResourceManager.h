#pragma once

#include <SFML/Graphics.hpp>

struct ResourceManager
{
    ResourceManager();
    
    sf::Texture textureAsteroid[7];
    sf::Texture textureBackground;
    sf::Texture textureShip;
    sf::Texture textureBullet;
};
