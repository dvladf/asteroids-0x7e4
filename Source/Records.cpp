#include "Records.h"
#include <iostream>

Records::Records(sf::RenderTarget& target)
{
    isModification = false;
    
    for (int i = 0; i < num_elements; ++i) {
        for (int j = 0; j < 10; ++j) {
            records[i].name[j] = ' ';
        }
        records[i].score = 0;
    }

    // read table of records
    in.open("records.dat", std::ios::binary | std::ios::in);
    if (in.is_open()) {
        size_t i = 0;
        while (i < num_elements || !in.eof()) {
            in.read(reinterpret_cast<char*> (&records[i]), sizeof(record));
            ++i;
        }
    }
    

    if (!font.loadFromFile("Data/kenvector_future.ttf")) {
        // error... File is not found
    }

    // settings for show table of records
    for (size_t i = 0; i < num_elements; i++) {
        tableRecordsScore[i].setFont(font);
        tableRecordsName[i].setFont(font);
        tableRecordsScore[i].setScale(target.getSize().x / 1200.f, target.getSize().x / 1200.f);
        tableRecordsName[i].setScale(target.getSize().x / 1200.f, target.getSize().x / 1200.f);
    }
}

Records::~Records()
{
    if (isModification) {
        out.open("records.dat", std::ios::binary | std::ios::trunc | std::ios::out);
        out.write(reinterpret_cast<char*> (&records), sizeof(records));
        out.close();
    }
}

void Records::show(sf::RenderTarget& target)
{
    std::stringstream string_record[num_elements][2];

    for (int i = 0; i < num_elements; i++) {
        if (records[i].score != 0) {
            string_record[i][0] << records[i].name;
            string_record[i][1] << records[i].score;

            tableRecordsName[i].setString(string_record[i][0].str());
            tableRecordsScore[i].setString(string_record[i][1].str());

            tableRecordsName[i].setPosition(target.getSize().x / 5.f, target.getSize().y / 5.f + 40.f * tableRecordsName[i].getScale().x *i);
            tableRecordsScore[i].setPosition(target.getSize().x / 1.5f, target.getSize().y / 5.f + 40.f * tableRecordsScore[i].getScale().x *i);

            tableRecordsScore[i].setColor(sf::Color::Yellow);
            target.draw(tableRecordsName[i]);
            target.draw(tableRecordsScore[i]);
        }
    }
}

void Records::add(std::string name, int score)
{
    for (int i = 0; i < num_elements; i++) {
        if (score > records[i].score) {
            for (int j = num_elements - 1; j > i; j--) {
                records[j] = records[j - 1];
            }
            records[i].score = score;
            for (int j = 0; j < 10; j++) {
                records[i].name[j] = name[j];
            }
            break;
        }
    }
    isModification = true;
}

int Records::getMinScore() const {
    return records[num_elements - 1].score;
}
