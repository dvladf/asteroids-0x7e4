#include "Game.h"
#include "Settings.h"

int main()
{
    Settings settings;
    Game game(settings);
    game.run();
}
