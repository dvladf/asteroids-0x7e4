#include "Settings.h"

std::fstream Settings::file;

Settings::Settings()
{
    std::string key; int arg;
    file.open(filename, std::ios::in);
    if (!file.is_open()) {
        setDefault();
        file.close();
        return;
    }

    while (!file.eof()) {
        file >> key >> arg;
        if (key == "fullscreen" && arg == 0) { fullscreen = false; }
        else if (key == "fullscreen" && arg == 1) { fullscreen = true;  }
        else if (key == "vertsync" && arg == 0) { vertsync = false; }
        else if (key == "vertsync" && arg == 1) { vertsync = true; }
        else if (key == "width") { resolution.x = arg; }
        else if (key == "height") { resolution.y = arg; }
        key = ""; arg = 0;
    }

	file.close();
}

Settings::~Settings()
{
    file.open(filename, std::ios::out | std::ios::trunc);
    file << "fullscreen" << '\t' << int(fullscreen) << '\n';
    file << "vertsync" << '\t' << int(vertsync) << '\n';
    file << "width" << '\t' << resolution.x << '\n';
    file << "height" << '\t' << resolution.y << '\n';
    file.close();
}

void Settings::setDefault()
{
    vertsync = true;
    fullscreen = true;
    resolution = { 0, 0 };
}
