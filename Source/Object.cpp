#include "Object.h"

Object::Object() {
    visibleObject = false;
}

Object::~Object()
{
}

void Object::add(sf::RenderTarget& target)
{
    spriteObject.setTexture(textureObject);
    spriteObject.setScale(target.getSize().x / 4200.f * scaleObject, target.getSize().x / 4200.f * scaleObject);
    spriteObject.setOrigin(textureObject.getSize().x / 2, textureObject.getSize().y / 2);
    spriteObject.setPosition(0, rand() % target.getSize().y);
    spriteObject.setRotation(rand() % 360);
    movementObject.x = sin(spriteObject.getRotation() * DEG2RAD);
    movementObject.y = -cos(spriteObject.getRotation() * DEG2RAD);
}

void Object::setTexture(sf::Texture change_texture) {
    textureObject = change_texture;
}

void Object::setSprite(sf::Sprite change_sprite) {
    spriteObject = change_sprite;
}

void Object::setMovement(sf::Vector2f change_movement){
    movementObject = change_movement;
}

void Object::setScale(float change_scale){
    scaleObject = change_scale;
}

void Object::setLife(int change_life){
    lifeObject = change_life;
}

void Object::setPoints(int change_points){
    pointsObject = change_points;
}

sf::Sprite Object::getSprite() const{
    return spriteObject;
}

sf::Vector2f Object::getMovement() const{
    return movementObject;
}

int Object::getLife() const{
    return lifeObject;
}

int Object::getPoints() const{
    return pointsObject;
}

void Object::show(sf::RenderTarget &target){
    target.draw(spriteObject);
}

float Object::fRand(float min, float max){
    float x = (float)rand() / RAND_MAX;
    return min + x * (max - min);
}

const float Asteroid::min_speed  = 0.1f;
const float Asteroid::max_speed  = 0.18f;
const float Asteroid::min_rotate = 0.5f;
const float Asteroid::max_rotate = 0.7f;

Asteroid::Asteroid(sf::Texture change_texture[])
{
    // set texture
    sf::Texture texture;
    int rand_num = rand() % 7;
    for (int i = 0; i < 7; i++) {
	if (rand_num == i) {
	    texture = change_texture[i];
	}
    }

    setTexture(texture);

    // set speed and rotate
    speed = fRand(min_speed, max_speed);
    rotate = fRand(min_rotate, max_rotate);

    if (rand() % 1 == 1) {
	rotate = -rotate;
    }
}

Asteroid::~Asteroid(){}

void Asteroid::update(float frametime)
{
    sf::Sprite sprite = getSprite();
    sprite.move(speed * frametime * getMovement());
    sprite.rotate(rotate);
    setSprite(sprite);
}

const int   BigAsteroid::life   = 6;
const int   BigAsteroid::points = 10;
const float BigAsteroid::scale  = 1.2f;

BigAsteroid::BigAsteroid(sf::Texture change_texture[]) : Asteroid(change_texture)
{
    setScale(scale);
    setLife(life);
    setPoints(points);
}

BigAsteroid::~BigAsteroid(){}

const int   MedAsteroid::life   = 3;
const int   MedAsteroid::points = 5;
const float MedAsteroid::scale  = 0.8f;

MedAsteroid::MedAsteroid(sf::Texture change_texture[]) : Asteroid(change_texture)
{
    setScale(scale);
    setLife(life);
    setPoints(points);
}

MedAsteroid::~MedAsteroid(){}

const int   SmallAsteroid::life   = 1;
const int   SmallAsteroid::points = 2;
const float SmallAsteroid::scale  = 0.5f;

SmallAsteroid::SmallAsteroid(sf::Texture change_texture[]) : Asteroid(change_texture)
{
    setScale(scale);
    setLife(life);
    setPoints(points);
}

SmallAsteroid::~SmallAsteroid()
{
}
