#include "Level.h"

Level::Level(sf::RenderTarget& target, ResourceManager& resourceManager):
    resourceManager(resourceManager)
{
    isLoading = false;

    hitSoundBuffer.loadFromFile("Data/sfx_hit.ogg");
    hitSound.setBuffer(hitSoundBuffer);

    player = new Player(target, resourceManager);
    score = 0;
    time_collision = 10;

    currentItemObjects = 0;
    currentCountObjects = countObjects;
    itemObjects = countObjects;
    rand_time = rand() % 1000 + 1000;
}

Level::~Level()
{
    delete player;
    player = nullptr;

    for (int i = 0; i < currentCountObjects; i++){
        delete objects[i];
        objects[i] = nullptr;
    }
}

void Level::input()
{
    player->input();
}

int Level::update(sf::RenderTarget& target,float frametime)
{
    isLoading = false;
    time = clock.getElapsedTime();
    if (time.asMilliseconds() > rand_time) {
        clock.restart();
        rand_time = rand() % 1000 + 1000;
        currentItemObjects++;
        itemObjects++;
    }

    if (itemObjects > countObjects - 1) {
        isLoading = true;
    } else {
        player->update(frametime);
        player->setSprite(checkFrontier(target, player->getSprite()));

        for (size_t i = 0; i < currentItemObjects; ++i) {
            objects[i]->update(frametime);
            objects[i]->setSprite(checkFrontier(target, objects[i]->getSprite()));
        }
        checkCollision(target);
    }
    return score;
}

bool Level::playerIsLive()
{
    if (time_collision == 0)
        return false;
    else
        return true;
}

bool Level::objectIsLoading() {
    return isLoading;
}

void Level::show(sf::RenderTarget& target)
{
    player->show(target);
    for (size_t i = 0; i < currentItemObjects; i++){
        objects[i]->show(target);
    }
}

sf::Sprite Level::checkFrontier(sf::RenderTarget& target, sf::Sprite sprite)
{
    if (sprite.getPosition().x > target.getSize().x) {
        sprite.setPosition(0, sprite.getPosition().y);

    } else if (sprite.getPosition().x < 0) {
        sprite.setPosition(target.getSize().x, sprite.getPosition().y);

    } else if (sprite.getPosition().y > target.getSize().y) {
        sprite.setPosition(sprite.getPosition().x, 0);

    } else if (sprite.getPosition().y < 0) {
        sprite.setPosition(sprite.getPosition().x, target.getSize().y);
    }
    return sprite;
}

void Level::checkCollision(sf::RenderTarget& target)
{
    for (int i = 0; i < currentItemObjects; i++) {
        if (player->getSprite().getGlobalBounds().intersects(objects[i]->getSprite().getGlobalBounds())){
            time_collision--;
        }

        for (int j = 0; j < player->getBullets().size(); j++) {
            if (player->getBullets().at(j)->getSprite().getGlobalBounds().intersects(objects[i]->getSprite().getGlobalBounds())) {
                player->eraseBullet(j);
                objects[i]->setLife(objects[i]->getLife() - 1);
                if (objects[i]->getLife() == 0) {
                    score = score + objects[i]->getPoints();
                    delete objects[i];
                    objects[i] = nullptr;

                    for (int a = i; a < countObjects - 1; a++) {
                        objects[a] = objects[a + 1];
                    }
                    currentItemObjects--;
                    currentCountObjects--;
                    hitSound.play();
                }
                break;
            }
        }
    }
}

void Level::loadObjects(sf::RenderTarget& target)
{
    for (int i = currentItemObjects; i < countObjects; i++) {
        int event = rand() % 2;
        if (event == 0){
            objects[i] = new SmallAsteroid(resourceManager.textureAsteroid);

        } else if (event == 1){
            objects[i] = new MedAsteroid(resourceManager.textureAsteroid);

        } else if (event == 2){
            objects[i] = new BigAsteroid(resourceManager.textureAsteroid);
        }

        objects[i]->add(target);
    }
    itemObjects = currentItemObjects;
}
