#include "Game.h"
#include <iostream>

Game::Game(Settings settings)
{
    if (settings.getFullscreen()) {
        window.create(sf::VideoMode::getDesktopMode(), "Asteroids 0x7E4", sf::Style::Fullscreen);
        window.setMouseCursorVisible(false);
    } else {
        window.create(sf::VideoMode(settings.getResolution().x, settings.getResolution().y), "Asteroids 0x7E4", sf::Style::Titlebar);
    }
    
    if (settings.getVertsync()) {
        window.setFramerateLimit(false);
        window.setVerticalSyncEnabled(true);
    } else {
        window.setFramerateLimit(app::fps);
        window.setVerticalSyncEnabled(false);
    }

    resourceManager.textureBackground.setRepeated(true);
    background.setTexture(resourceManager.textureBackground);
    background.setTextureRect(sf::IntRect(0, 0, window.getSize().x, window.getSize().y));

    window.setKeyRepeatEnabled(true);

    if (!font.loadFromFile("Data/kenvector_future.ttf")) {
        // error... File is not found
    }

    text_version.setFont(font);
    text_version.setPosition(window.getSize().x - 100, 10);
    text_version.setScale(font::size::small);
    text_version.setString(app::version);

    text_score.setFont(font);
    text_score.setPosition(10, 10);
    text_score.setColor(sf::Color::Cyan);
    text_score.setScale(font::size::medium);

    text_loading.setFont(font);
    text_loading.setColor(sf::Color::Cyan);
    text_loading.setScale(font::size::medium);
    text_loading.setString("Loading...");
    text_loading.setOrigin((text_loading.getLocalBounds().width) / 2, (text_loading.getLocalBounds().height) / 2);
    text_loading.setPosition(window.getSize().x / 2 , window.getSize().y - 60);

    loseSoundBuffer.loadFromFile("Data/sfx_lose.ogg");
    loseSound.setBuffer(loseSoundBuffer);

    music.openFromFile("Data/music.ogg");
    music.setVolume(20);
    music.setLoop(true);

    records = new Records(window);
    isNewGame = true;
}

Game::~Game()
{
    delete records;
    records = nullptr;
    if (level) delete level;
    level = nullptr;
}

void Game::run()
{
    while (window.isOpen()) {
        if (runMenu()) {
            isNewGame = false;
            level = new Level(window, resourceManager);
            runLevel();
            delete level;
            level = nullptr;
            isNewGame = true;
            continue;
        }
    }
}

bool Game::runMenu()
{
    Menu menu(window, isNewGame);
    int select = -1;
    if (!isNewGame) {
        music.pause();
    }

    while (true) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            menu.input();
        }
        select = menu.update();

        window.clear();
        window.draw(background);
        window.draw(text_version);

        if (!isNewGame) {
            level->show(window);
        }

        if (select == 0) { return true; }
        else if (select == 1) { runTableRecords(); }
        else if (select == 2) { window.close(); return false; }

        menu.show(window);

        clock.restart();
        window.display();
    }
}

void Game::runLevel()
{
    music.play();

    bool isRun = true;
    while (isRun) {
        sf::Event event;
        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }

            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
                if (runMenu() == false) { isRun = false; }
                music.play();
            }

            level->input();
        }

        score = level->update(window, clock.restart().asMilliseconds());

        std::stringstream string_score;
        string_score << "score: " << score;
        text_score.setString(string_score.str());

        window.clear();
        window.draw(background);
        level->show(window);
        window.draw(text_score);

        if (level->objectIsLoading()) {
            window.draw(text_loading);
            window.display();
            level->loadObjects(window);
            clock.restart();
        } else {
            window.display();
        }

        if (!level->playerIsLive()) {
            runGameOver();
            isRun = false;
        }
    }
}

void Game::runGameOver()
{
    loseSound.play();
    music.stop();
    sf::Text text_gameover[2];
    text_gameover[0].setString("Game Over");
    text_gameover[1].setString("Please  press  'enter'  for  continue");

    for (size_t i = 0; i < 2; ++i) {
        text_gameover[i].setFont(font);
        text_gameover[i].setOrigin(text_gameover[i].getLocalBounds().width / 2, text_gameover[i].getLocalBounds().height / 2);
    }

    text_gameover[0].setScale(font::size::large);
    text_gameover[0].setPosition(window.getSize().x / 2, window.getSize().y / 2);
    text_gameover[0].setColor(sf::Color::Red);
    text_gameover[1].setScale(0.7, 0.7);
    text_gameover[1].setPosition(window.getSize().x / 2, window.getSize().y - 50);
    text_gameover[1].setColor(sf::Color::Cyan);

    bool isRun = true;
    while (isRun) {
        sf::Event event;

        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) { window.close(); }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) { isRun = false; }
        }

        window.clear();
        window.draw(background);
        window.draw(text_gameover[0]);
        window.draw(text_gameover[1]);
        window.display();
    }

    clock.restart();
    while (clock.getElapsedTime().asSeconds() < 0.2);

    if (score > records->getMinScore()) {
        runAddRecord();
    }
}

void Game::runAddRecord()
{
    bool isRun = true;
    sf::Text text_addRecord[3];

    text_addRecord[0].setString("Congratulations!  You  have  a  new  record!  Enter  your  name:");
    text_addRecord[0].setScale(window.getSize().x / 1500.f, window.getSize().x / 1500.f);

    text_addRecord[2].setString("Please  press  'enter'  for  continue");
    text_addRecord[2].setScale(font::size::medium);
    text_addRecord[2].setPosition(window.getSize().x / 2, window.getSize().y - 50);
    text_addRecord[2].setColor(sf::Color::Cyan);

    std::string name;

    for (int i = 0; i < 3; i++) {
        text_addRecord[i].setFont(font);
        text_addRecord[i].setOrigin(text_addRecord[i].getLocalBounds().width / 2, text_addRecord[i].getLocalBounds().height / 2);
    }

    text_addRecord[0].setPosition(window.getSize().x / 2, 50);
    text_addRecord[0].setColor(sf::Color::Yellow);

    text_addRecord[1].setScale(1.2, 1.2);
    text_addRecord[1].setPosition(window.getSize().x / 2, window.getSize().y / 2);

    while (isRun) {
        sf::Event event;

        while (window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) {
                window.close();
            }
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)) {
                isRun = false;
            }

            if (event.type == sf::Event::TextEntered) {
                // Handle ASCII characters only
                if (event.text.unicode >= 41 && event.text.unicode <= 125) {
                    name += static_cast<char>(event.text.unicode);
                    text_addRecord[1].setString(name);
                    text_addRecord[1].setOrigin(text_addRecord[1].getLocalBounds().width / 2, 0);
                } else if (event.text.unicode == '\b' && !name.empty()) {
                    name.pop_back();
                    text_addRecord[1].setString(name);
                    text_addRecord[1].setOrigin(text_addRecord[1].getLocalBounds().width / 2, 0);
                }
            }
        }

        window.clear();
        window.draw(background);
        window.draw(text_addRecord[0]);
        window.draw(text_addRecord[1]);
        window.draw(text_addRecord[2]);
        window.display();
    }

    if (name.size() == 0) { name = "Anonimous"; }

    for (size_t i = name.size(); i < 10; i++) {
        name.push_back(' ');
    }

    records->add(name, score);
    clock.restart();
    while (clock.getElapsedTime().asSeconds() < 0.2);
}

void Game::runTableRecords()
{
    bool isRun = true;
    sf::Text tableRecords[2];

    tableRecords[0].setString("Table of Records");
    tableRecords[1].setString("Please  press  'enter'  for  continue");

    for (int i = 0; i < 2; i++) {
        tableRecords[i].setFont(font);
        tableRecords[i].setOrigin(tableRecords[i].getLocalBounds().width / 2, tableRecords[i].getLocalBounds().height / 2);
    }
    
    tableRecords[0].setPosition(window.getSize().x / 2, 50);
    tableRecords[0].setColor(sf::Color::Red);

    tableRecords[1].setScale(0.7, 0.7);
    tableRecords[1].setPosition(window.getSize().x / 2, window.getSize().y - 50);
    tableRecords[1].setColor(sf::Color::Cyan);

    sf::Event event;

    while (isRun) {
        while (window.pollEvent(event)){
            if (event.type == sf::Event::Closed)
            window.close();
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return)){
                isRun = false;
                break;
            }
        }
        window.clear();
        window.draw(background);
        window.draw(tableRecords[0]);
        window.draw(tableRecords[1]);
        records->show(window);
        window.display();
    }

    clock.restart();
    while (clock.getElapsedTime().asSeconds() < 0.2);
}
