#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "Bullet.h"
#include "ResourceManager.h"

#define DEG2RAD 3.14159f / 180.0f
#define DEF_SPEED 0.0005f
#define DEF_ANGLE 0.2f
#define TIME_SLEEP_SHOT 10

class Player
{
public:
    Player(sf::RenderTarget& target, ResourceManager& resourceManager);
    ~Player();

    void input();
    void update(float frametime);
    void show(sf::RenderTarget& target);
    void eraseBullet(int i);

    sf::Sprite getSprite() const;
    sf::Vector2f getMovement() const;
    std::vector<Bullet*> getBullets() const;
    void setSprite(sf::Sprite change_sprite);
    void setMovement(sf::Vector2f change_movement);
    void setBullets(std::vector<Bullet*> change_bullets);

private:
    void shot();
    void checkFrontierBullets(sf::RenderTarget& target);

private:
    ResourceManager& resourceManager;
    sf::Sprite player;

    sf::Vector2f movement;
    float accleration, angle;

    bool checkShot;
    bool isAccleration;
    std::vector<Bullet*> bullets;

    sf::SoundBuffer shotSoundBuffer;
    sf::Sound shotSound;
};
