## Install dependencies:

Debian/Ubuntu:
```
$ sudo apt install libsfml-dev cmake
```

## Build

Linux (g++):
```
$ mkdir build
$ cd build
$ cmake ..
$ make
```

## Controls

In menu:
```
Next item - down arrow
Previous item - up arrow
Choose - enter
```

In game:
```
Up - up arrow or w
Down - down arrow or s
Left rotate - left arrow or a
Right rotate - right arrow or d
Fire - space
Esc - menu
```